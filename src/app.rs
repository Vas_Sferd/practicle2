use slint::{Model, ModelRc, VecModel, SharedPixelBuffer, SharedString};
use rfd::FileDialog;
use plotters::prelude::*;

use std::rc::Rc;

use crate::service::Service;

use super::{
    config::Config,
    domain::{
        currency::Rates,
        weather::{Forecast, OneDayForecast},
    },
    service,
    state::{
        State,
        DomainData,
    },
    tool::eprint_err::{
        BoxedError,
        EprintErr
    },
};

slint::slint! {
    import { MainWindow } from "src/ui/main.slint";
}

impl Into<DomainData> for MainModel {
    fn into(self) -> DomainData {
        DomainData {
            rates: self.rates().clone(),
            forecast: self.forecast().clone(),
        }
    }
}

impl State for MainModel {
    fn from_domain_data(from: DomainData) -> Self {
        let weather_plot_points = from.forecast().iter()
        .map(|d| {
            WeatherPlotPoint {
                clouds: d.clouds as i32,
                day_temperature: d.day_temperature,
            }
        }).collect::<Vec<_>>();
        
        let weather_plot_indexes: Vec<i32> = (0..(*&from.forecast().iter().count() as i32)).collect();

        MainModel {
            currency_value: from.rates(),
            weather_data: Rc::new(VecModel::from(weather_plot_points)).into(),
            weather_indexes: Rc::new(VecModel::from(weather_plot_indexes)).into()
        }
    }

    fn rates(&self) -> Rates {
        self.currency_value
    }

    fn forecast<'a>(&'a self) -> Forecast {
        self.weather_data.iter().map(|w| OneDayForecast {
                clouds: w.clouds as u32,
                day_temperature: w.day_temperature
        }).collect::<Forecast>()
    }
}

pub struct Application {
    window: MainWindow,
    conf: Config,
    state: Rc<VecModel<MainModel>>,
    weatherapi_service: Rc<service::weatherapi::Service>,
    cbr_json_service: Rc<service::cbr_json::Service>,
}

pub fn indexes_with_nones(values: Vec<i32>) -> Vec<Option<i32>> {
    let max = values.last().map(|x| *x).unwrap_or(0);
    (0..=max).into_iter().map(|x| if values.contains(&x) { Some(x) } else { None }).collect()
}

fn try_parse_indexes_text(text: &str, range: std::ops::Range<i32>) -> Option<Vec<i32>> {
    let iox: Vec<Option<i32>> = if text.trim().is_empty() {
        range.clone().into_iter().map(|x| Some(x)).collect()
    } else {
        (&text).split(',').into_iter()
            .map(|s| s.trim())
            .map(|x| x.parse::<i32>().ok())
            .collect()
    };
    
    if (&iox).iter().all(|o| o.is_some()) {
        let ix: Vec<i32> = iox.iter().map(|x| x.expect("[Unreachable Error] All data must be correct i32")).collect();
        if ix.iter().all(|item| (&range).contains(item)) {
            Some(ix)
        } else {
            None
        }
    } else {
        None
    }
}

impl Application {
    pub fn try_create(conf: Config) -> Option<Application> {
        let cbr_json_service = service::cbr_json::Service::create(&conf.cbr_json);
        let weatherapi_service = service::weatherapi::Service::create(&conf.weatherapi);
        let window = MainWindow::new();

        let domain_data = DomainData::read_from_file(&conf.storage_file_path)
            .eprint_err("Failed open to get state from file")
            .or_else(|| DomainData::download_from_apis(&cbr_json_service, &weatherapi_service)
            .eprint_err("Failed to create state"))?;

        Some(Application {
            window,
            conf,
            state: Rc::new(VecModel::from(vec![MainModel::from_domain_data(domain_data)])),
            weatherapi_service: Rc::new(weatherapi_service),
            cbr_json_service: Rc::new(cbr_json_service),
        })
    }

    pub fn run(self) -> Result<(), Box<dyn std::error::Error>> {
        let weatherapi_service = self.weatherapi_service.clone();
        let cbr_json_service = self.cbr_json_service.clone();
        
        self.window.set_app_model(self.state.clone().into());
        self.window.on_render_clouds_plot(Application::render_clouds_plot);
        self.window.on_render_temperature_plot(Application::render_temperature_plot);
        self.window.on_load_file(Application::load_file);
        self.window.on_save_file(Application::save_file);
        self.window.on_reload(move |model| Application::reload_from_api(model, &weatherapi_service, &cbr_json_service));
        self.window.on_edit_indexes(Application::edited_indexes_check);
        self.window.on_set_indexes(Application::edited_indexes_update);
        self.window.run();
        self.exit()
    }

    pub fn edited_indexes_update(model: ModelRc<MainModel>, text: SharedString) -> SharedString {
        let model_data = model.iter().next().expect("[Unreachable Error] model must be initialized");
        SharedString::from(
            match try_parse_indexes_text(
                &text,
                0..(model_data.forecast().len() as i32)
                ) {
                Some(ix) => {
                    model.set_row_data(
                        0,
                        MainModel {
                            weather_indexes: Rc::new(VecModel::from(ix)).into(),
                            ..model_data
                        }
                    );
                    "Нажмите Enter, чтобы увидеть изменения на графике"
                }
                None => "[Error] Введены некорректные данные"
            }
        )
    }
    
    pub fn edited_indexes_check(model: MainModel, text: SharedString) -> SharedString {
        SharedString::from(
            match try_parse_indexes_text(&text, 0..(model.forecast().len() as i32)) {
                Some(_) => {
                    "Нажмите Enter, чтобы увидеть изменения на графике"
                }
                None => "[Error] Введены некорректные данные"
            }
        )
    }

    pub fn load_file(model: ModelRc<MainModel>) -> SharedString {
        SharedString::from(
            if let Some(file_path) = FileDialog::new()
            .set_title("Открыть файл")
            .add_filter("ron", &["ron"])
            .add_filter("xml", &["xml"])
            .add_filter("json", &["json"])
            .set_directory(".")
            .pick_file() {
                if let Some(extension) = (&file_path).extension() {
                    if let Some(utf_extension) = extension.to_str() {
                        if ["ron", "xml", "json"].contains(&utf_extension) {
                            if let Some(domain_data) = DomainData::read_from_file(&file_path) {
                                model.set_row_data(
                                    0,
                                    MainModel::from_domain_data(
                                        domain_data
                                    )
                                );
                                "Успешно загружены данные из файла"
                            } else { "[Error] Выбранный файл поврежден" }
                        } else { "[Error] Расширение {} выбранного файла не поддерживается" }
                    } else { "[Error] Некорректные символы в имени выбранного файла" }
                } else { "[Error] Выбранный файл не имеет расширения" }
            } else { "[Error] Ошибка создания файлового диалога" }
        )
    }

    pub fn save_file(model: MainModel, file_format: SharedString) -> SharedString {
        SharedString::from(
            if let Some(file_path) = FileDialog::new()
            .set_title("Выберете файл для сохранения")
            .add_filter(&file_format, &[&file_format])
            .set_directory(".")
            .save_file() {
                let file_path_with_extension = match (&file_path).extension().map(|ext| ext.to_str()) {
                    Some(Some(utf_extension)) => if utf_extension != (&file_format).as_str() {
                            file_path.with_extension((&file_format).as_str())
                        } else {
                            file_path
                        }
                    _ => file_path.with_extension((&file_format).as_str())
                };

                let data: DomainData = model.into();
                if let Ok(_) = data.write_to_file(file_path_with_extension) {
                    "Данные обновлены успешно"
                } else {
                    "[Error] Ошибка записи файла" 
                }
            } else { "[Error] Ошибка создания файлового диалога" }
        )
    }

    pub fn reload_from_api(model: ModelRc<MainModel>, weatherapi_service: &service::weatherapi::Service, cbr_json_service: &service::cbr_json::Service) -> SharedString {
        SharedString::from(  
            match DomainData::download_from_apis(&cbr_json_service, &weatherapi_service) {
                Some(domain_data) => {
                    model.set_row_data(
                        0,
                        MainModel::from_domain_data(
                            domain_data
                        )
                    );
                    "Успешно обновлены данные через веб интерфейс"
                },
                None => "[Error] Ошибка получения данных по сети"
            }    
        )
    }

    pub fn render_clouds_plot(points_model: ModelRc<WeatherPlotPoint>, indexes_model: ModelRc<i32>) -> slint::Image {
        let indexes = indexes_with_nones(indexes_model.iter().collect());
        let points: Vec<_> = points_model.iter().collect();

        let data: Vec<_> = points.iter().zip(indexes.iter()).map(
            |(data, op)| {
                match op {
                    Some(_) => data.clouds as u32,
                    None => 0u32,
                }
            }
        ).collect();
        let data_len = data.len() as u32;

        let mut pixel_buffer = SharedPixelBuffer::new(640, 480);
        let size = (pixel_buffer.width(), pixel_buffer.height());

        let backend = BitMapBackend::with_buffer(pixel_buffer.make_mut_bytes(), size);
    
        let root = backend.into_drawing_area();
        root.fill(&WHITE).expect("error filling drawing area");

        let mut chart = ChartBuilder::on(&root)
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("Прогноз облачности", ("sans-serif", 42.0))
            .build_cartesian_2d((0u32..5u32).into_segmented(), 0u32..100u32)
            .expect("Cant build char");

        chart
            .configure_mesh()
            .disable_x_mesh()
            .bold_line_style(&WHITE.mix(0.1))
            .y_desc("Мера облачности")
            .x_desc("День прогноза")
            .axis_desc_style(("sans-serif", 15))
            .draw()
            .expect("Cant draw chart");

        let data_iter = ((0u32..data_len).into_segmented().values()).zip(data.iter().map(|x| *x)).collect::<Vec<_>>();
        let line_series = LineSeries::new(
            data_iter,
            &RED,
        );
        
        chart.draw_series(line_series).expect("Cant draw chart series");

        root.present().expect("error presenting");
        drop(chart);
        drop(root);

        slint::Image::from_rgb8(pixel_buffer)
    }

    pub fn render_temperature_plot(points_model: ModelRc<WeatherPlotPoint>, indexes_model: ModelRc<i32>) -> slint::Image {
        let indexes = indexes_with_nones(indexes_model.iter().collect());
        let points: Vec<_> = points_model.iter().collect();

        let data: Vec<_> = points.iter().zip(indexes.iter()).map(
            |(data, op)| {
                match op {
                    Some(_) => data.day_temperature as f64,
                    None => 0f64,
                }
            }
        ).collect();
        let data_len = data.len() as u32;

        let mut pixel_buffer = SharedPixelBuffer::new(640, 480);
        let size = (pixel_buffer.width(), pixel_buffer.height());

        let backend = BitMapBackend::with_buffer(pixel_buffer.make_mut_bytes(), size);
    
        let root = backend.into_drawing_area();
        root.fill(&WHITE).expect("error filling drawing area");

        let mut chart = ChartBuilder::on(&root)
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("Прогноз температуры", ("sans-serif", 42.0))
            .build_cartesian_2d((0u32..5u32).into_segmented(), -45f64..50f64)
            .expect("Cant build char");

        chart
            .configure_mesh()
            .disable_x_mesh()
            .bold_line_style(&WHITE.mix(0.1))
            .y_desc("°C")
            .x_desc("День прогноза")
            .axis_desc_style(("sans-serif", 15))
            .draw()
            .expect("Cant draw chart");

        let data_iter = ((0u32..data_len).into_segmented().values()).zip(data.iter().map(|x| *x)).collect::<Vec<_>>();
        let line_series = LineSeries::new(
            data_iter,
            &RED,
        );
        
        chart.draw_series(line_series).expect("Cant draw chart series");

        root.present().expect("error presenting");
        drop(chart);
        drop(root);

        slint::Image::from_rgb8(pixel_buffer)
    }

    fn exit(self: Application) -> Result<(), Box<dyn std::error::Error>> {
        #[derive(Debug)]
        struct StateCorruptedError;

        impl std::error::Error for StateCorruptedError {}

        impl std::fmt::Display for StateCorruptedError {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(f, "State is corrupted")
            }
        } 
        
        let cache: DomainData = self
            .state
            .row_data(0)
            .ok_or(StateCorruptedError)?
            .into();
        
        cache.write_to_file(&self.conf.storage_file_path)
            .map_err(|e| BoxedError::from(e))
            .eprint_err("Cant save state to file")
            .map_err(|e| e.error_box())
    }
}

