use ron::{de::from_reader, ser::to_string};
use serde::{Deserialize, Serialize};
use std::{
    fs::{write, File},
    path::{Path, PathBuf},
};

use crate::{
    service::{self, cbr_json, weatherapi},
    tool::eprint_err::EprintErr,
};

#[derive(Clone, Default, Debug, Deserialize, Serialize)]
pub struct Config {
    pub cbr_json: <cbr_json::Service as service::Service>::Params,
    pub weatherapi: <weatherapi::Service as service::Service>::Params,
    pub storage_file_path: PathBuf,
}

impl Config {
    #[allow(dead_code)]
    pub fn save_default(name: impl AsRef<Path>) -> Result<(), Box<dyn std::error::Error>> {
        Self::default().write_to_file(name)
    }

    pub fn write_to_file(&self, name: impl AsRef<Path>) -> Result<(), Box<dyn std::error::Error>> {
        write(name, to_string(self)?).map_err(|e| e.into())
    }

    pub fn read_from_file(name: impl AsRef<Path>) -> Option<Self> {
        from_reader(File::open(name).ok().eprint_err("Failed open conf file")?)
            .eprint_err("Failed parsing file")
            .ok()
    }
}
