use std::str::FromStr;

use serde::{Deserialize, Serialize};

/// Курс валюты
pub type Rates = f32;

/// Название валюты
#[derive(Clone, PartialEq, Eq, Debug, Deserialize, Serialize)]
pub struct Currency(String);

impl Default for Currency {
    fn default() -> Self {
        Self("<currency>".to_string())
    }
}

impl FromStr for Currency {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Currency(s.to_string()))
    }
}

impl ToString for Currency {
    fn to_string(&self) -> String {
        self.0.clone()
    }
}
