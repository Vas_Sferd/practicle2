use serde::{Deserialize, Serialize};

/// Параметры прогноза погоды
#[derive(Clone, Eq, PartialEq, Debug, Deserialize, Serialize)]
pub struct ForecastParams {
    pub city_name: String,
    pub day_count: u32,
}

impl Default for ForecastParams {
    fn default() -> Self {
        Self {
            city_name: "<city name>".to_string(),
            day_count: 7,
        }
    }
}

/// Прогноз погоды на несколько дней
pub type Forecast = Box<[OneDayForecast]>;

/// Прогноз погоды только на один день
#[derive(Clone, Copy, PartialEq, Debug, Deserialize, Serialize)]
pub struct OneDayForecast {
    pub day_temperature: f32,
    pub clouds: u32,
}
