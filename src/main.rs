mod app;
mod config;
mod domain;
mod service;
mod state;
mod tool;

use config::Config;
use app::Application;

fn main() -> Result<(), Box<dyn std::error::Error>>{
    let conf_path = std::env::var("GOLDEN_WEATHER_CONFIG").unwrap_or("./conf.ron".to_owned());

    if let Some(conf) = Config::read_from_file(&conf_path) {
        Application::try_create(conf).expect("failed to create application").run()
    } else {
        eprint!("Config file not found in \"{}\"\nYou can specify config path in variable GOLDEN_WEATHER_CONFIG", conf_path);
        Config::save_default("conf.ron.example")
    }
}
