use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::domain::currency;

pub struct Service(currency::Currency);

impl super::Service for Service {
    type Out = currency::Rates;
    type Params = currency::Currency;

    fn create(params: &Self::Params) -> Self {
        Service(params.clone())
    }

    fn call(&self) -> Result<Self::Out, Box<dyn std::error::Error>> {
        #[derive(Debug, Deserialize, Serialize)]
        struct Response {
            #[serde(rename = "Valute")]
            valute: HashMap<String, ValuteValue>,
        }

        #[derive(Debug, Deserialize, Serialize)]
        struct ValuteValue {
            #[serde(rename = "Value")]
            value: currency::Rates,
        }

        #[derive(Debug)]
        struct MismatchedCurrencyError;

        impl std::error::Error for MismatchedCurrencyError {}

        impl std::fmt::Display for MismatchedCurrencyError {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(f, "Mismatched valute char code")
            }
        }

        let response = reqwest::blocking::get("https://www.cbr-xml-daily.ru/daily_json.js")?
            .json::<Response>()?;

        if let Some(valute_value) = response.valute.get(self.0.to_string().as_str()) {
            Ok(valute_value.value)
        } else {
            Err(MismatchedCurrencyError.into())
        }
    }
}
