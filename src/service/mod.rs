pub mod cbr_json;
pub mod weatherapi;

pub trait Service {
    type Out;
    type Params;

    fn create(params: &Self::Params) -> Self;

    fn call(&self) -> Result<Self::Out, Box<dyn std::error::Error>>;
}
