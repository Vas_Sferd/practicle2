use serde::{Deserialize, Serialize};

use crate::domain::weather;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Params {
    pub api_key: String,
    pub domain_params: weather::ForecastParams,
}

impl Default for Params {
    fn default() -> Self {
        Self {
            api_key: "<api_key>".to_string(),
            domain_params: weather::ForecastParams::default(),
        }
    }
}

pub struct Service(Params);

impl super::Service for Service {
    type Out = weather::Forecast;
    type Params = Params;

    fn create(params: &Self::Params) -> Self {
        Self(params.clone())
    }

    fn call(&self) -> Result<Self::Out, Box<dyn std::error::Error>> {
        #[derive(Debug, Deserialize, Serialize)]
        struct Response {
            forecast: ForecastList,
        }

        #[derive(Debug, Deserialize, Serialize)]
        struct ForecastList {
            forecastday: Vec<ForecastHours>,
        }

        #[derive(Debug, Deserialize, Serialize)]
        struct ForecastHours {
            hour: Vec<ForecastInHourData>,
        }

        #[derive(Copy, Clone, Debug, Deserialize, Serialize)]
        struct ForecastInHourData {
            temp_c: f32,
            cloud: u32,
        }

        let response: Response = reqwest::blocking::get(format!(
            "https://api.weatherapi.com/v1/forecast.json?key={}&q={}&days={}&aqi=no&alerts=no",
            self.0.api_key, self.0.domain_params.city_name, self.0.domain_params.day_count,
        ))?
        .json()?;

        Ok(
            response
                .forecast
                .forecastday
                .into_iter()
                .map(|xs| xs.hour[13])
                .map(|data| weather::OneDayForecast {
                    clouds: data.cloud,
                    day_temperature: data.temp_c,
                })
                .collect(),
        )
    }
}
