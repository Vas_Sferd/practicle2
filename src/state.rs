use ron::{de::from_reader as ron_from_reader, ser::to_string as ron_to_string};
use quick_xml::{de::from_reader as xml_from_reader, se::to_string as xml_to_string};
use serde_json::{de::from_reader as json_from_reader, ser::to_string as json_to_string};

use std::fs::{write, File};
use std::path::Path;

use serde::{Deserialize, Serialize};

use crate::{
    domain::{
        currency::Rates,
        weather::Forecast,
    },
    service::{
        cbr_json,
        weatherapi,
        Service,
    },
    tool::eprint_err::{
        BoxedError,
        EprintErr,
    },
};

pub trait State : Clone + Into<DomainData> {
    fn from_domain_data(from: DomainData) -> Self;
    
    fn rates(&self) -> Rates;
    fn forecast(&self) -> Forecast;
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DomainData {
    pub rates: Rates,
    pub forecast: Forecast,
}

impl State for DomainData {
    fn from_domain_data(from: DomainData) -> Self {
        from
    }

    fn rates(&self) -> Rates {
        self.rates
    }

    
    fn forecast(&self) -> Forecast {
        self.forecast.clone()
    }
}

impl DomainData {
    pub fn download_from_apis(currency_service: &cbr_json::Service, weather_service: &weatherapi::Service) -> Option<Self> {
        Some(Self {
            rates: currency_service
                .call()
                .ok()
                .eprint_err("Rates valute data not loaded")?,
            forecast: weather_service
                .call()
                .ok()
                .eprint_err("Forecast not loaded")?
                .into(),
        })
    }

    pub fn write_to_file(&self, name: impl AsRef<Path>) -> Result<(), Box<dyn std::error::Error>> {
        let content = match name.as_ref().extension().map(|ext| ext.to_str()) {
            Some(Some("ron")) => ron_to_string(self).eprint_err("Cant convert data to ron")?,
            Some(Some("xml")) => xml_to_string(self).eprint_err("Cant convert data to xml")?,
            Some(Some("json")) => json_to_string(self).eprint_err("Cant convert data to json")?,
            _ => Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Incorrect file extension"))?
        };
        
        write(name, content).map_err(|e| e.into())
    }

    pub fn read_from_file(name: impl AsRef<Path>) -> Option<Self> {
        let file = File::open(&name).ok().eprint_err("Failed open content file")?;

        match name.as_ref().extension().map(|ext| ext.to_str()) {
            Some(Some("ron")) => ron_from_reader(file).map_err(|e| BoxedError::boxing(e)),
            Some(Some("xml")) => xml_from_reader(std::io::BufReader::new(file)).map_err(|e| BoxedError::boxing(e)),
            Some(Some("json")) => json_from_reader(file).map_err(|e| BoxedError::boxing(e)),
            _ => Err(BoxedError::boxing(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Incorrect file extension")))
        }.eprint_err("Failed parsing file")
        .ok()
    }
}