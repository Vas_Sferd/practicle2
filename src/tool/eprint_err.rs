mod is_error {
    pub trait IsError {
        fn is_error(&self) -> bool;
        fn error_description(&self) -> Option<String> {
            None
        }
    }

    impl<T> IsError for Option<T> {
        fn is_error(&self) -> bool {
            self.is_none()
        }
    }

    impl<T, E: std::error::Error> IsError for Result<T, E> {
        fn is_error(&self) -> bool {
            self.is_err()
        }
        fn error_description(&self) -> Option<String> {
            if let Err(e) = self {
                Some(format!("{}", e))
            } else {
                None
            }
        }
    }
}

pub trait EprintErr {
    fn eprint_err(self, message: &str) -> Self;
}

impl<T: is_error::IsError> EprintErr for T {
    #[inline]
    fn eprint_err(self, message: &str) -> Self {
        if self.is_error() {
            eprint!("[Error:] {}\n", &message);
            if let Some(description) = self.error_description() {
                eprint!("[Error details:] {}\n", description)
            }
        };

        self
    }
}

#[derive(Debug)]
pub struct BoxedError(Box<dyn std::error::Error>);

impl BoxedError {
    pub fn boxing<E: std::error::Error + 'static>(e: E) -> BoxedError {
        let boxed_value: Box<dyn std::error::Error> = Box::new(e).into();
        BoxedError::from(boxed_value)
    }
    
    pub fn error_box(self) -> Box<dyn std::error::Error> {
        self.0
    }
}

impl From<Box<dyn std::error::Error>> for BoxedError {
    fn from(b: Box<dyn std::error::Error>) -> Self {
        Self(b)
    }
}

impl std::fmt::Display for BoxedError {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(fmt, "{}", self.0.as_ref())
    }
}

impl std::error::Error for BoxedError {}
